#pragma once
#include "Renderer.h"
#include "object.h"
#include "Globals.h"
#include "Physics.h"
#include "Sound.h"

class ScnMgr
{
public:
	ScnMgr();
	~ScnMgr();

	void RenderScene( float eTimeInSecond );
	void Update(float eTimeInSecond);
	void DoGarbageCollection();

	int AddObject(float x, float y, float z,
		float sx, float sy, float sz,
		float r, float g, float b, float a,
		float vx, float vy, float vz, 
		float mass, float fricCoef,
		float hp,
		float type,
		float bounce);
	void DeleteObject(int idx);

	void KeyDownInput(unsigned char key, int x, int y);		//x,y - mouse input
	void KeyUpInput(unsigned char key, int x, int y);

	void SpecialKeyDownInput( int key, int x, int y );
	void SpecialKeyUpInput( int key, int x, int y );

private:
	Renderer* m_Renderer = NULL;
	object* m_obj[MAX_OBJ_COUNT];
	Physics* m_Physics = NULL;
	Sound* m_Sound = NULL;

	bool m_KeyW = false;
	bool m_KeyS = false;
	bool m_KeyA = false;
	bool m_KeyD = false;
	bool m_KeySP = false;


	bool m_KeyUp = false;
	bool m_KeyDown = false;
	bool m_KeyLeft = false;
	bool m_KeyRight = false;

	int m_Texture = -1;
	int m_TitleTex = -1;
	int m_EndTex = -1;
	int m_AnimTex = -1;
	int m_BG = -1;
	int m_Particle = -1;
	int m_Particleobj = -1;

	int m_SoundBG = -1;
	int m_SoundFire = -1;

	int m_GameState = TITLE_STATE;
};

