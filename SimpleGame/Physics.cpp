#include "stdafx.h"
#include "Physics.h"

Physics::Physics() {

}
Physics::~Physics() {

}

bool Physics::IsCollide( object* A, object* B, int type ) {
	switch (type) {
	case 0:
		// BB Collision
		return BBCollision( A, B );
		break;
	case 1:
		break;
	}
}

bool Physics::BBCollision( object* A, object* B ) {
	float x, y, z;
	float sx, sy, sz;
	float minX, minY, minZ;
	float maxX, maxY, maxZ;
	float x1, y1, z1;
	float sx1, sy1, sz1;
	float minX1, minY1, minZ1;
	float maxX1, maxY1, maxZ1;

	// cals min, max of A


	A->GetPos( &x, &y, &z );
	A->GetVol( &sx, &sy, &sz );
	B->GetPos( &x1, &y1, &z1 );
	B->GetVol( &sx1, &sy1, &sz1 );

	minX = x - sx / 2.f; minY = y; minZ = z - sz / 2.f;
	maxX = x + sx / 2.f; maxY = y + sy; maxZ = z + sz / 2.f;

	minX1 = x1 - sx1 / 2.f; minY1 = y1 - sy1 / 2.f; minZ1 = z1 - sz1 / 2.f;
	maxX1 = x1 + sx1 / 2.f; maxY1 = y1 + sy1 / 2.f; maxZ1 = z1 + sz1 / 2.f;

	if (minX > maxX1)
		return false;
	if (maxX < minX1)
		return false;

	if (minY > maxY1)
		return false;
	if (maxY < minY1)
		return false;

	if (minZ > maxZ1)
		return false;
	if (maxZ < minZ1)
		return false;


	return true;
}

void Physics :: ProcessCollision( object* A, object* B ) {

	// obj A
	float vx, vy, vz;
	float mass;
	A->GetVel( &vx, &vy, &vz );
	A->GetMass( &mass );

	// obj B
	float vx1, vy1, vz1;
	float mass1;
	B->GetVel( &vx1, &vy1, &vz1 );
	B->GetMass( &mass1 );

	// calc A
	float vfx, vfy, vfz;
	vfx = ((mass - mass1) / (mass + mass1)) * vx
		+ ((2 * mass1) / (mass + mass1)) * vx1;
	vfy = ((mass - mass1) / (mass + mass1)) * vy
		+ ((2 * mass1) / (mass + mass1)) * vy1;
	vfz = ((mass - mass1) / (mass + mass1)) * vz
		+ ((2 * mass1) / (mass + mass1)) * vz1;

	// calc B
	float vfx1, vfy1, vfz1;
	vfx1 = ((2 * mass) / (mass + mass1)) * vx
		+ ((mass1 - mass) / (mass + mass1)) * vx1;
	vfy1 = ((2 * mass) / (mass + mass1)) * vy
		+ ((mass1 - mass) / (mass + mass1)) * vy1;
	vfz1 = ((2 * mass) / (mass + mass1)) * vz
		+ ((mass1 - mass) / (mass + mass1)) * vz1;

	A->SetVel( vfx, vfy, vfz );
	B->SetVel( vfx1, vfy1, vfz1 );
}