#pragma once

#include "Globals.h"

class object
{
public:
	object();
	~object();
	void SetPos(float x, float y, float z);
	void GetPos(float* x, float* y, float* z);

	void SetVol(float sx, float sy, float sz);
	void GetVol(float* sx, float* sy, float* sz);

	void SetVel(float sx, float sy, float sz);
	void GetVel(float* sx, float* sy, float* sz);

	void SetAcc(float sx, float sy, float sz);
	void GetAcc(float* sx, float* sy, float* sz);

	void SetColor(float r, float g, float b, float a);
	void GetColor(float* r, float* g, float* b, float* a);

	void SetMass(float mass);
	void GetMass(float* mass);

	void SetFricCoef(float mass);
	void GetFricCoef(float* mass);

	void SetType(int type);
	void GetType(int* type);

	void SetTex( int id );
	void GetTex( int* id );

	void SetHP( float hp );
	void GetHP( float* hp );
	void Damage( float damage );

	void AddForce(float x, float y, float z, float eTimeInSecond);

	void InitThis();

	void Update(float eTimeInSecond);

	bool CanShootBullet();
	void ResetBulletCoolTime();

	void SetParentObj( object* parent );
	bool IsAncestor( object* obj );

	void SetBounced( int );
	void GetBounced(int*);

private:
	float m_r, m_g, m_b, m_a;	    //color
	float m_posX, m_posY, m_posZ;   //position
	float m_mass;					//mass
	float m_velX, m_velY, m_velZ;	//velocity
	float m_accX, m_accY, m_accZ;	//acceleration
	float m_volX, m_volY, m_volZ;	//volume
	float m_fricCoef;				//Friction coefficient
	int m_type;					// object type
	float m_healthPoint;			// HP
	int m_bounced = 0;				// ���� ƨ��°�?

	float m_remainingCoolTime = 0.f;
	float m_bulletCoolTime = 0.2f;
	
	int m_tex_ID = -1;

	

	object* m_parent;
};

