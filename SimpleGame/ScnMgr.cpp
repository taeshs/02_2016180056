#include "stdafx.h"
#include "ScnMgr.h"
#include "Dependencies\freeglut.h"
#include <random>
#include <windows.h>

int cnt = 0;

ScnMgr::ScnMgr() {
	// Initialize Renderer
	m_Renderer = new Renderer(500, 500);
	if (!m_Renderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}

	/*m_TestObj = new object();
	m_TestObj->SetLocation(0, 0, 0);
	m_TestObj->SetSize(10, 10, 10);
	m_TestObj->SetColor(1, 1, 1, 1);*/

	// initialize m_Physics
	m_Physics = new Physics();
	m_Sound = new Sound();

	//Initialize m_obj
	for (int i = 0; i < MAX_OBJ_COUNT; i++) {
		m_obj[i] = NULL;
	}

	m_obj[HERO_ID] = new object();
	m_obj[HERO_ID]->SetPos(0,0,0);
	m_obj[HERO_ID]->SetVel(0.0, 0, 0);
	m_obj[HERO_ID]->SetVol(0.8,0.8,0.8);
	m_obj[HERO_ID]->SetColor(1,1,1,1);
	m_obj[HERO_ID]->SetMass(10);
	m_obj[HERO_ID]->SetFricCoef(0.7);
	m_obj[HERO_ID]->SetHP( 1000 );
	m_BG =
		m_Renderer->GenPngTexture( "./Textures/bg.png" );
	m_Texture =
		m_Renderer->GenPngTexture(
			"./Textures/shopkeeper.png"
		);
	m_TitleTex = m_Renderer->GenPngTexture(
		"./Textures/title.png"
	);
	m_EndTex = m_Renderer->GenPngTexture(
		"./Textures/end.png"
	);
	m_AnimTex =
		m_Renderer->GenPngTexture(
			"./Textures/skeleton.png"
		);
	m_Particle =
		m_Renderer->GenPngTexture(
			"./Textures/shopkeeper.png"
		);
	m_obj[HERO_ID]->SetTex( m_AnimTex );

	m_SoundBG = m_Sound->CreateBGSound("./Sounds/game.mp3");
	m_Sound->PlayBGSound( m_SoundBG, true, 1.f );
	m_SoundFire = m_Sound->CreateShortSound( "./Sounds/lightning.mp3" );

	int monster[3];
	random_device rd;
	default_random_engine dre{ rd() };
	uniform_int_distribution<int> uid( -4, 4 );
	for (int i = 0; i < 3; ++i) {
		monster[i] = AddObject(uid(dre), uid( dre ) ,0,
			0.5,0.5,0.5,
			1,1,1,1,
			0,0,0,
			10,
			0.7,
			10,
			TYPE_NORMAL,0);
		m_obj[monster[i]]->SetTex( m_Texture );
	}
	// ��
	int temp = AddObject( 0, 5, 0,
		10, 0.5, 0.5,
		1, 0.5, 0, 1,
		0, 0, 0,
		9999,
		0.7,
		10000000,
		TYPE_WALL,0 );

	int temp1 = AddObject( 0, -5, 0,
		10, 0.5, 0.5,
		1, 0.5, 0, 1,
		0, 0, 0,
		9999,
		0.7,
		10000000,
		TYPE_WALL,0 );

	int temp2 = AddObject( 5, -5, 0,
		0.5, 10.5, 0.5,
		1, 0.5, 0, 1,
		0, 0, 0,
		9999,
		0.7,
		10000000,
		TYPE_WALL,0 );

	int temp3 = AddObject( -5, -5, 0,
		0.5, 10.5, 0.5,
		1, 0.5, 0, 1,
		0, 0, 0,
		9999,
		0.7,
		10000000,
		TYPE_WALL,0 );

	m_Particleobj = m_Renderer->CreateParticleObject(
		1,
		0,0,
		0,0,
		3,3,
		7,7,
		-30,-30,
		30,30
		);
}

ScnMgr::~ScnMgr() {

}

void ScnMgr::KeyDownInput( unsigned char key, int x, int y ) {
	if (m_GameState == MAIN_STATE) {
		if (key == 'w' || key == 'W') {
			m_KeyW = true;
		}
		else if (key == 's' || key == 'S') {
			m_KeyS = true;
		}
		else if (key == 'a' || key == 'A') {
			m_KeyA = true;
		}
		else if (key == 'd' || key == 'D') {
			m_KeyD = true;
		}
		else if (key == ' ') {
			m_KeySP = true;
		}
		else if (key == 'q' || key == 'Q') {
			m_GameState = TITLE_STATE;
		}
	}
	else if (m_GameState == TITLE_STATE) {
		m_GameState = MAIN_STATE;
	}
	else {
		if (key == 'q' || key == 'Q') {
			exit( 0 );
		}
	}
}

void ScnMgr::KeyUpInput(unsigned char key, int x, int y) {
	if (m_GameState == MAIN_STATE) {
		if (key == 'w' || key == 'W') {
			m_KeyW = false;
		}
		else if (key == 's' || key == 'S') {
			m_KeyS = false;
		}
		else if (key == 'a' || key == 'A') {
			m_KeyA = false;
		}
		else if (key == 'd' || key == 'D') {
			m_KeyD = false;
		}
		else if (key == ' ') {
			m_KeySP = false;
		}
		else if (key == 'q' || key == 'Q') {
			m_GameState = TITLE_STATE;
		}
	}
	else if (m_GameState == TITLE_STATE) {
		m_GameState = MAIN_STATE;
	}
	else {
		if (key == 'q' || key == 'Q') {
			exit( 0 );
		}
	}
}

void ScnMgr::SpecialKeyDownInput( int key, int x, int y ) {
	if (m_GameState == MAIN_STATE) {
		if (key == GLUT_KEY_UP) {
			m_KeyUp = true;
		}
		else if (key == GLUT_KEY_DOWN) {
			m_KeyDown = true;
		}
		else if (key == GLUT_KEY_LEFT) {
			m_KeyLeft = true;
		}
		else if (key == GLUT_KEY_RIGHT) {
			m_KeyRight = true;
		}
	}
	else if (m_GameState == TITLE_STATE) {
		m_GameState = MAIN_STATE;
	}
}
void ScnMgr::SpecialKeyUpInput( int key, int x, int y ) {
	if (m_GameState == MAIN_STATE) {
		if (key == GLUT_KEY_UP) {
			m_KeyUp = false;
		}
		else if (key == GLUT_KEY_DOWN) {
			m_KeyDown = false;
		}
		else if (key == GLUT_KEY_LEFT) {
			m_KeyLeft = false;
		}
		else if (key == GLUT_KEY_RIGHT) {
			m_KeyRight = false;
		}
	}
}

void ScnMgr::RenderScene( float eTimeInSecond ) {
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glClearColor( 0.0f, 0.3f, 0.3f, 1.0f );

	if (m_GameState == TITLE_STATE) {
		m_Renderer->DrawTextureRect( 0,-150,0, 600, 300, 0, 1,1,1,1 , m_TitleTex);
	}
	else if (m_GameState == MAIN_STATE) {
		m_Renderer->DrawGround(
			0, 0, 0,
			1000, 1000, 0,
			1, 1, 1, 1,
			m_BG,
			1.f
		);
		static float pTime = 0.f;
		pTime += 0.016f;


		for (int i = 0; i < MAX_OBJ_COUNT; i++) {
			if (m_obj[i] != NULL) {
				float x, y, z;
				float sx, sy, sz;
				float r, g, b, a;
				int texID;
				int type;
				float hp;
				m_obj[i]->GetHP( &hp );
				m_obj[i]->GetPos( &x, &y, &z );
				x = 100.f * x;	//convert to pixel size
				y = 100.f * y;
				z = 100.f * z;
				m_obj[i]->GetVol( &sx, &sy, &sz );
				sx = 100.f * sx;	//convert to pixel size
				sy = 100.f * sy;
				sz = 100.f * sz;
				m_obj[i]->GetColor( &r, &g, &b, &a );
				m_obj[i]->GetTex( &texID );
				m_obj[i]->GetType( &type );
				//m_Renderer->DrawSolidRect(x, y, z, sx, r, g, b, a);

				if (type == TYPE_NORMAL) {
					m_Renderer->DrawSolidRectGauge(
						x, y, z,
						0, sy / 2.f, 0,
						sx, 5, 0,
						1, 0, 0, 1,
						(hp / 10.f) * 100
					);
				}

				static int temp = 0;
				int ix = temp % 12;
				if (i == HERO_ID) {
					m_Renderer->DrawTextureRectAnim(
						x, y, z, sx, sy, sz, r, g, b, a, texID, 6, 1, ix / 2, 0
					); temp += eTimeInSecond;
					m_Renderer->DrawParticle(
						m_Particleobj,
						x, y, z,
						1,
						1, 1, 1, 1,
						-100, -100,
						m_Particle,
						1.f,
						pTime,
						0.f
					);
				}
				else {
					if (type == TYPE_NORMAL) {
						m_Renderer->DrawTextureRect( x, y, z, sx, sy, sz, r, g, b, a, texID );
					}
					else {
						m_Renderer->DrawSolidRect( x, y, z, sx, sy, sz, r, g, b, a );
					}
					
				}
			}
		}
	}
	else {
		m_Renderer->DrawTextureRect( 0, -150, 0, 600, 300, 0, 1, 1, 1, 1, m_EndTex );
		//Sleep( 1000 );
		//exit( 0 );
	}
}

void ScnMgr::Update(float eTimeInSecond) {
	float px = 0.f;
	float py = 0.f;
	float pz = 0.f;

	m_obj[HERO_ID]->GetPos( &px, &py, &pz );

	if (!(px<5.f && px > -5.f && py < 5.f && py > -5.f)) {
		m_obj[HERO_ID]->SetPos( 0.f, 0.f, 0.f );
	}
	//echo current key input state
	/*std::cout << "W:" << m_KeyW
		<< "S:" << m_KeyS
		<< "A:" << m_KeyA
		<< "D:" << m_KeyD << std::endl;*/
	/*std::cout << "UP:" << m_KeyUp
		<< "DOWN:" << m_KeyDown
		<< "LEFT:" << m_KeyLeft
		<< "RIGHT:" << m_KeyRight << std::endl;*/
	float fx = 0.f;
	float fy = 0.f;
	float fz = 0.f;
	float fAmount = 200.f;

	if (m_KeyW) fy += 1.f;
	if (m_KeyS) fy -= 1.f;
	if (m_KeyA) fx -= 1.f;
	if (m_KeyD) fx += 1.f;
	if (m_KeySP) fz += 1.f;

	float fSize = sqrtf( fx * fx + fy * fy );
	if (fSize > FLT_EPSILON) {
		fx /= fSize;
		fy /= fSize;
		fx *= fAmount;
		fy *= fAmount;

		m_obj[HERO_ID]->AddForce( fx, fy, fz, eTimeInSecond );
	}

	if (fz > FLT_EPSILON) {
		fz *= fAmount * 10.f;

		float hx, hy, hz;
		m_obj[HERO_ID]->GetPos( &hx, &hy, &hz );
		if(hz<FLT_EPSILON)
			m_obj[HERO_ID]->AddForce( 0, 0, fz, eTimeInSecond );
	}

	// Shoot Missile
	float bulvx, bulvy, bulvz;
	bulvx = bulvy = bulvz = 0.f;
	if (m_KeyUp) bulvy += 1.f;
	if (m_KeyDown) bulvy -= 1.f;
	if (m_KeyLeft) bulvx -= 1.f;
	if (m_KeyRight) bulvx += 1.f;
	float bulVSize = sqrtf( bulvx * bulvx + bulvy * bulvy );
	if (bulVSize > 0.f && m_obj[HERO_ID]->CanShootBullet()) {
		float bulletSpeed = 5.f;
		bulvx = bulletSpeed * bulvx / bulVSize;
		bulvy = bulletSpeed * bulvy / bulVSize;

		float heroX, heroY, heroZ;
		float heroVX, heroVY, heroVZ;
		m_obj[HERO_ID]->GetPos( &heroX, &heroY, &heroZ );
		m_obj[HERO_ID]->GetVel( &heroVX, &heroVY, &heroVZ );

		//bulvx = bulvx + heroVX;
		//bulvy = bulvy + heroVY;
		//bulvz = bulvz + heroVZ;

		int idx = AddObject(
			heroX, heroY + 0.4, heroZ,
			0.07, 0.07, 0.07,
			1, 0, 0, 1,
			heroVX, heroVY, heroVZ,				//bulvx , vy ,vz
			0.7f,
			0.1f,
			1.f,
			TYPE_BULLET,0
		);
		m_obj[idx]->AddForce( bulvx, bulvy, bulvz, 1.3 );
		m_obj[idx]->SetParentObj( m_obj[HERO_ID] );
		m_obj[HERO_ID]->ResetBulletCoolTime();
		m_Sound->PlayShortSound( m_SoundFire, false, 1.f );
	}

	// collision detect			- bullet n bullet - ignore , bullet n normal - ź���浹
	for (int source = 0; source < MAX_OBJ_COUNT; source++) {
		for (int target = source + 1; target < MAX_OBJ_COUNT; target++) {
			if (m_obj[source] != NULL &&
				m_obj[target] != NULL) {
				if (m_Physics->IsCollide(
					m_obj[source],
					m_obj[target] )) {
					int srcType; m_obj[source]->GetType( &srcType );
					int trgType; m_obj[target]->GetType( &trgType );
					if (srcType == TYPE_WALL || trgType == TYPE_WALL) {
						if (!(source == HERO_ID || target == HERO_ID)) {
							int b;
							m_Physics->ProcessCollision(
								m_obj[source],
								m_obj[target] );
							if (srcType == TYPE_BULLET) {
								m_obj[source]->GetBounced( &b );
								m_obj[source]->SetBounced( b + 1 );
								std::cout << b << std::endl;
							}
							if (trgType == TYPE_BULLET) {
								m_obj[target]->GetBounced( &b );
								m_obj[target]->SetBounced( b + 1 );
								std::cout << b << std::endl;
							}
						}
					}
					else if (!m_obj[source]->IsAncestor( m_obj[target] )
						&& !m_obj[target]->IsAncestor( m_obj[source] ) && !(srcType == TYPE_BULLET && trgType == TYPE_BULLET)
						) {
						int b;
						std::cout << "collision : " << source << ", " << target << std::endl;
						m_Physics->ProcessCollision(
							m_obj[source],
							m_obj[target] );
						if (srcType == TYPE_BULLET) {
							m_obj[source]->GetBounced( &b );
							float srcHP, dstHP;
							m_obj[source]->GetHP( &srcHP );
							m_obj[target]->GetHP( &dstHP );
							float fSrcHP = srcHP - dstHP;
							float fDstHP = dstHP - srcHP *b;
							m_obj[source]->SetHP( fSrcHP );
							m_obj[target]->SetHP( fDstHP );
						}
						else if (trgType == TYPE_BULLET) {
							m_obj[target]->GetBounced( &b );
							float srcHP, dstHP;
							m_obj[source]->GetHP( &srcHP );
							m_obj[target]->GetHP( &dstHP );
							float fSrcHP = srcHP - dstHP *b;
							float fDstHP = dstHP - srcHP;
							m_obj[source]->SetHP( fSrcHP );
							m_obj[target]->SetHP( fDstHP );
						}
					}
				}
			}
		}
	}

	for (int i = 0; i < MAX_OBJ_COUNT; i++) {
		if (m_obj[i] != NULL) {
			m_obj[i]->Update(eTimeInSecond);
		}
	}

	float x, y, z;
	m_obj[HERO_ID]->GetPos( &x, &y, &z );
	m_Renderer->SetCameraPos( x*100, y*100 );
}
void ScnMgr::DoGarbageCollection() {
	for (int i = 0; i < MAX_OBJ_COUNT; i++) {
		if (m_obj[i] != NULL) {
			int type;
			m_obj[i]->GetType( &type );
			if (type == TYPE_BULLET) {
				float vx, vy, vz;
				m_obj[i]->GetVel( &vx, &vy, &vz );
				float vSize =
					sqrtf(
						vx * vx + vy * vy + vz * vz
					);
				if (vSize < FLT_EPSILON) {
					DeleteObject( i );
					continue;
				}
			}
			float hp;
			m_obj[i]->GetHP( &hp );
			if (hp < FLT_EPSILON) {
				DeleteObject( i );
				if (type == TYPE_NORMAL) {
					cnt++;
					if (cnt == 3) {
						m_GameState = END_STATE;
						m_obj[HERO_ID]->SetPos( 0.f, 0.f, 0.f );
					}
				}
				continue;
			}
		}
	}
}

int  ScnMgr::AddObject( float x, float y, float z,
	float sx, float sy, float sz,
	float r, float g, float b, float a,
	float vx, float vy, float vz,
	float mass, float fricCoef,
	float hp,
	float type,
	float bounce )
{
	//Search empty slot idx
	int idx = -1;
	for (int i = 0; i < MAX_OBJ_COUNT; i++) {
		if (m_obj[i] == NULL)
		{
			idx = i;
			break;
		}
	}

	if (idx == -1) {
		std::cout << "No more empty slot." << std::endl;
		return -1;
	}

	m_obj[idx] = new object();
	m_obj[idx]->SetPos(x, y, z);
	m_obj[idx]->SetVel(vx, vy, vz);
	m_obj[idx]->SetVol(sx, sy, sz);
	m_obj[idx]->SetColor(r, g, b, a);
	m_obj[idx]->SetMass( mass );
	m_obj[idx]->SetFricCoef( fricCoef );
	m_obj[idx]->SetHP( hp );
	m_obj[idx]->SetType( type );
	m_obj[idx]->SetBounced( bounce );

	return idx;
}

void ScnMgr::DeleteObject(int idx) {
	if (idx < 0){
		std::cout << "Input idx is negative : " << idx << std::endl;
		return;
	}

	if (idx >= MAX_OBJ_COUNT) {
		std::cout << "Input idx exceeds MAX_OBJ_COUNT : " << idx << std::endl;
		return;
	}

	if (m_obj[idx] == NULL) {
		std::cout << "m_obj[" << idx <<"] is NULL."<< std::endl;
		return;
	}

	delete m_obj[idx];
	m_obj[idx] = NULL;
}