#pragma once
#include "object.h"

class Physics
{
public:
	Physics();
	~Physics();

	bool IsCollide( object* A, object* B, int type = 0 );
	void ProcessCollision( object* A, object* B );

private: 
	bool BBCollision( object* A, object* B );
};

