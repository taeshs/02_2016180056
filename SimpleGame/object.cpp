#include "stdafx.h"
#include "object.h"
#include "math.h"
#include <float.h>

object::object() {
	InitThis();
}

object::~object() {

}

void object::InitThis() {
	m_posX = 0; m_posY = 0; m_posZ = -1.f;
	m_volX = 0; m_volY = 0; m_volZ = -1.f;
	m_velX = 0; m_velY = 0; m_velZ = 0;
	m_accX = 0; m_accY = 0; m_accZ = -1.f;
	m_mass = -1.f;
	m_r = 1; m_g = 1; m_b = 1; m_a = 1;
	m_fricCoef = 0.f;
}

void object::AddForce(float x, float y, float z, float eTimeInSecond) {
	float accX = x / m_mass;
	float accY = y / m_mass;
	float accZ = z / m_mass;

	m_velX = m_velX + accX * eTimeInSecond;
	m_velY = m_velY + accY * eTimeInSecond;
	m_velZ = m_velZ + accZ * eTimeInSecond;
}
bool object::CanShootBullet() {
	if (m_remainingCoolTime < FLT_EPSILON) {
		return true;
	}
	return false;

}

void object::ResetBulletCoolTime() {
	m_remainingCoolTime = m_bulletCoolTime;
}

void object::Update(float eTimeInSecond) {
	// reduce bullet cool time
	m_remainingCoolTime -= eTimeInSecond;

	//Apply friction////////////////////////////////////////////////////
	float Fn = GRAVITY * m_mass;			// 수직항력의 크기
	float Friction = m_fricCoef * Fn;		// 마찰력의 크기

	// normalize velocity cevtor for getting direction of object
	float dirX, dirY, dirZ;							// 진행방향 단위벡터
	float velSize = sqrtf(m_velX * m_velX + m_velY * m_velY + m_velZ * m_velZ );

	if (velSize > FLT_EPSILON) {
		dirX = m_velX / velSize;
		dirY = m_velY / velSize;			// 정규화
		dirZ = m_velZ / velSize;

		//calculate friction force
		float frictionX = -dirX * Friction;
		float frictionY = -dirY * Friction;

		//calculate friction acc
		float accX = frictionX / m_mass;
		float accY = frictionY / m_mass;
		float accZ = -GRAVITY;

		//update velocity by friction force
		float newVelX = m_velX + accX * eTimeInSecond;
		float newVelY = m_velY + accY * eTimeInSecond;
		float newVelZ = m_velZ + accZ * eTimeInSecond;

		if (newVelX * m_velX < 0.f) {
			m_velX = 0.f;
		}
		else {
			m_velX = newVelX;
		}

		if (newVelY * m_velY < 0.f) {
			m_velY = 0.f;
		}
		else {
			m_velY = newVelY;
		}

		m_velZ = newVelZ;
	
	}
	else if(m_posZ > FLT_EPSILON) {
		float accZ = -GRAVITY;
		float newVelZ = m_velZ + accZ * eTimeInSecond;
		m_velZ = newVelZ;
	}
	else
	{
		m_velX = 0.f;
		m_velY = 0.f;
		m_velZ = 0.f;
	}

	////////////////////////////////////////////////////////////////////

	//Update position
	m_posX = m_posX + m_velX * eTimeInSecond;
	m_posY = m_posY + m_velY * eTimeInSecond;
	m_posZ = m_posZ + m_velZ * eTimeInSecond;

	if (m_posZ < FLT_EPSILON) {
		m_posZ = 0.f;
		m_velZ = 0.f;
	}
}

void object::SetPos(float x, float y, float z) {
	m_posX = x;
	m_posY = y;
	m_posZ = z;
}
void object::GetPos(float* x, float* y, float* z) {
	*x = m_posX;
	*y = m_posY;
	*z = m_posZ;
}
void object::SetVol(float sx, float sy, float sz) {
	m_volX = sx;
	m_volY = sy;
	m_volZ = sz;
}
void object::GetVol(float* sx, float* sy, float* sz) {
	*sx = m_volX;
	*sy = m_volY;
	*sz = m_volZ;
}
void object::SetVel(float sx, float sy, float sz) {
	m_velX = sx;
	m_velY = sy;
	m_velZ = sz;
}
void object::GetVel(float* sx, float* sy, float* sz) {
	*sx = m_velX;
	*sy = m_velY;
	*sz = m_velZ;
}
void object::SetAcc(float sx, float sy, float sz) {
	m_accX = sx;
	m_accY = sy;
	m_accZ = sz;
}
void object::GetAcc(float* sx, float* sy, float* sz) {
	*sx = m_accX;
	*sy = m_accY;
	*sz = m_accZ;
}

void object::SetMass(float mass) {
	m_mass = mass;
}
void object::GetMass(float* mass) {
	*mass = m_mass;
}

void object::SetFricCoef(float coef) {
	m_fricCoef = coef;
}
void object::GetFricCoef(float* coef) {
	*coef = m_fricCoef;
}

void object::SetColor(float r, float g, float b, float a) {
	m_r = r;
	m_g = g;
	m_b = b;
	m_a = a;
}
void object::GetColor(float* r, float* g, float* b, float* a) {
	*r = m_r;
	*g = m_g;
	*b = m_b;
	*a = m_a;
}

void object::SetType( int type ) {
	m_type = type;
}
void object::GetType( int* type ) {
	*type = m_type;
}

void object::SetTex( int id ) {
	m_tex_ID = id;
}
void object::GetTex( int* id ) {
	*id = m_tex_ID ;
}

void object::SetParentObj( object* obj ) {
	m_parent = obj;
}
bool object::IsAncestor( object* obj ) {
	if (obj != NULL) {
		if (obj == m_parent) {
			return true;
		}
	}
	return false;
}

void object::SetHP( float hp ) {
	m_healthPoint = hp;
}
void object::GetHP( float* hp ) {
	*hp = m_healthPoint;
}
void object::Damage( float damage ) {
	m_healthPoint = m_healthPoint - damage;
}

void object::SetBounced( int b ) {
	m_bounced = b;
}
void object::GetBounced( int* b ) {
	*b = m_bounced;
}